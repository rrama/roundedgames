# README #

Please note this repository's **code has not been updated since early 2014!**
This API extends the [Games](https://bitbucket.org/rrama/games) API.

## Content ##

In this repository is an API for easily coding round based mini-games into the game Minecraft.

* In-game commands (e.g. forcing the round to start).
* Handling ingame events (e.g. late joiners).
* Handling round states.
* And more!

## Author ##
[rrama](https://bitbucket.org/rrama/) (Ben Durrans).