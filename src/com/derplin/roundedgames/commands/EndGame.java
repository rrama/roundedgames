package com.derplin.roundedgames.commands;

import com.derplin.roundedgames.Rounded;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class EndGame implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Bukkit.broadcastMessage(ChatColor.DARK_AQUA + "Game ended prematurely.");
        Rounded.callRoundEnd();
        return true;
    }
    
}