package com.derplin.roundedgames.commands;

import com.derplin.roundedgames.Rounded;
import org.bukkit.command.CommandExecutor;

/**
 * Calls for registering {@link CommandExecutor}s are in this class.
 */
public class RegisterRoundedCmds {
    
    /**
     * Registers all the commands.
     */
    public static void register() {
        reg(new EndGame(), "EndGame");
    }
    
    /**
     * Registers a command.
     * @param ce the CommandExecutor class to register the command labels to.
     * @param cmds the command label.
     */
    private static void reg(CommandExecutor ce, String... cmds) {
        for (String cmd : cmds) {
            Rounded.plugin.getCommand(cmd).setExecutor(ce);
        }
    }
    
}