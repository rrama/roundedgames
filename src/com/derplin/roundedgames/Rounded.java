package com.derplin.roundedgames;

import com.derplin.games.Games;
import com.derplin.games.commands.Ref;
import com.derplin.games.gameutil.CountDown;
import com.derplin.games.gameutil.CountDownNull;
import com.derplin.games.gameutil.MapManager;
import com.derplin.roundedgames.commands.RegisterRoundedCmds;
import com.derplin.roundedgames.events.RegisterRoundedEvents;
import com.derplin.roundedgames.events.WaitingForPlayers;
import java.io.IOException;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main class for the RoundedGames library.
 *
 * @author rrama, Lonesface and Arrem
 *         All rights reserved to rrama, Lonesface and Arrem. Use by Derplin only.
 *         No copying/stealing any part of the code (Exceptions; You have written consent from a member of the Derplin development team).
 *         No copying/stealing ideas from the code (Exceptions; You have written consent from a member of the Derplin development team).<br />
 *         <p/>
 *         Credit goes to rrama (author), Lonesface (author), Arrem (author)
 */
public class Rounded extends JavaPlugin {

    public static Rounded plugin;
    private static CountDown currentCount;
    /* Calling plugin should edit variables below. */
    public static RoundManager roundManager;
    public static int minimumPlayers = 1;
    public static int roundTime = -1;

    @Override
    public void onDisable() {
        Bukkit.getScheduler().cancelTasks(plugin);
    }

    @Override
    public void onEnable() {
        plugin = this;
        
        currentCount = new CountDownNull();
        
        RegisterRoundedEvents.register();
        RegisterRoundedCmds.register();
    }
    
    /**
     * Loads the next map.
     * Server will die if there are no maps.
     */
    public static void callAfterLoad() {
        String map = roundManager.selectNextMap();
        if (map == null) {
            map = MapManager.getRandomMap();
            if (map == null) {
                Bukkit.getLogger().severe(ChatColor.DARK_RED + "No maps found! Server dies now. x_x");
                Bukkit.shutdown();
                return;
            }
        }

        try {
            Bukkit.getLogger().info(ChatColor.DARK_AQUA + "Loading next game's map '" + map + "' now. Inb4 kippers.");
            Games.loadMap(Games.game, map);
        } catch (IOException ex) {
            Bukkit.getLogger().severe(ChatColor.DARK_RED + "Unable to load map '" + map + "'.\n"
                                      + ex.getMessage() + "\nServer dies now. x_x");
            Bukkit.shutdown();
        }
    }
    
    /**
     * Called by {@link MainWorldLoad} and {@link WaitingForPlayers}.
     * <ol>
     * <li>Calls {@link WaitingForPlayers} if there are not enough {@link Player}s.</li>
     * <li>Calls <code>RoundManager#roundStart()</code>.</li>
     * <li>Starts the end game {@link CountDown}.</li>
     * </ol>
     */
    public static void tryStartGame() {
        if (Bukkit.getOnlinePlayers().length - Ref.getOnlineRefs().size() < minimumPlayers) {
            WaitingForPlayers.register();
            return;
        }
        Bukkit.getWorlds().get(0).setTime(1000);
        try {
            roundManager.roundStart();
        } catch (Exception ex) {
            Bukkit.broadcastMessage(ChatColor.DARK_RED + "Call to roundStart() threw an Exception! Server dies now. x_x");
            Bukkit.getLogger().log(Level.SEVERE, "Call to roundStart() threw an Exception.", ex);
            Bukkit.shutdown();
        }

        if (roundTime != -1) {
            currentCount = new CountDown(roundTime, ChatColor.DARK_AQUA + "Game ends in %d seconds.", new Runnable() {

                @Override
                public void run() {
                    callRoundEnd();
                }

            });
        }
    }
    
    /**
     * <ol>
     * <li>Cancels the current {@link CountDown}.</li>
     * <li>Calls <code>RoundManager#roundEnd()</code>.</li>
     * <li>Shuts the server down.</li>
     * </ol>
     */
    public static void callRoundEnd() {
        if (!(currentCount instanceof CountDownNull)) {
            currentCount.cancel();
            try {
                roundManager.roundEnd();
            } catch (Exception ex) {
                Bukkit.getLogger().log(Level.WARNING, "Call to roundEnd() threw an Exception", ex);
            }
        }
        Bukkit.broadcastMessage(ChatColor.DARK_AQUA + "Server shutting down! You should be teleported to the lobby.");
        Bukkit.shutdown();
    }
    
}