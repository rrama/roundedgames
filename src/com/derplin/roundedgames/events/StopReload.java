package com.derplin.roundedgames.events;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class StopReload implements Listener {
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        String msg = event.getMessage().toLowerCase();
        if (msg.matches("/stop($|\\s.*)")) {
            event.getPlayer().sendMessage(ChatColor.YELLOW + "Can't use /stop. Try using /EndGame.");
            event.setCancelled(true);
        } else if (msg.matches("/reload($|\\s.*)")) {
            event.getPlayer().sendMessage(ChatColor.YELLOW + "Can't use /reload, it would break the game.\nIf you want to end the game use /EndGame.");
            event.setCancelled(true);
        }
    }
    
}