package com.derplin.roundedgames.events;

import com.derplin.games.commands.Ref;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class LateArrivals implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerLogin(PlayerLoginEvent event) {
        if (!event.getPlayer().hasPlayedBefore()) {
            if (event.getPlayer().isOp()) {
                Ref.addRef(event.getPlayer());
            } else {
                event.disallow(Result.KICK_WHITELIST, "Too late to join, the round has already stared.");
            }
        }
    }
    /**
     * Calls the registering of the LateArrivals {@link Listener}.
     */
    public static void register() {
        RegisterRoundedEvents.reg(new LateArrivals());
    }
    
}