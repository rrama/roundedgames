package com.derplin.roundedgames.events;

import com.derplin.games.commands.Ref;
import com.derplin.games.events.extra.RefRemoveEvent;
import com.derplin.roundedgames.Rounded;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.scheduler.BukkitTask;

/**
 * A {@link Listener} that calls <code>Rounded#loadNextMap()</code>
 * when enough {@link Player}s join to meet the minimum requirement.
 * The {@link Event} automatically unregisters itself.
 */
public class WaitingForPlayers implements Listener {
    
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerLogin(PlayerLoginEvent event) {
        if (event.getPlayer().isOp()) {
            Ref.addRef(event.getPlayer());
        }
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (Bukkit.getOnlinePlayers().length - Ref.getOnlineRefs().size() < Rounded.minimumPlayers) {
            broadcastMinimum();
            event.getPlayer().teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
        } else {
            unregister();
            Rounded.tryStartGame();
        }
    }
    
    private static void broadcastMinimum() {
        Bukkit.broadcastMessage(ChatColor.GREEN + "Waiting for more players. Minimum players: " + Rounded.minimumPlayers + ".");
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockSpread(BlockSpreadEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageEvent(EntityDamageEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        event.setCancelled(true);
    }
    
    private static BukkitTask announce;
    private static WaitingForPlayers waitingForPlayers;
    
    /**
     * Calls the registering of the WaitingForPlayers {@link Listener}.
     */
    public static void register() {
        waitingForPlayers = new WaitingForPlayers();
        RegisterRoundedEvents.reg(waitingForPlayers);
        announce = Bukkit.getScheduler().runTaskTimerAsynchronously(Rounded.plugin, new Runnable() {

            @Override
            public void run() {
                broadcastMinimum();
            }
            
        }, 0, 1200);
    }
    
    /**
     * Calls the unregistering of the WaitingForPlayers {@link Listener}.
     */
    public static void unregister() {
        RegisterRoundedEvents.unreg(waitingForPlayers);
        waitingForPlayers = null;
        announce.cancel();
    }
    
}