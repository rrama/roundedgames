package com.derplin.roundedgames.events;

import com.derplin.roundedgames.Rounded;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

/**
 * Calls for registering {@link Event}s are in this class.
 */
public class RegisterRoundedEvents {
    
    /**
     * Registers all the always-running {@link Event}s.
     */
    public static void register() {
        reg(new StopReload());
    }
    
    /**
     * Registers the {@link Event} {@link Listener}.
     * @param ll the {@link Listener} to register.
     */
    protected static void reg(Listener ll) {
        Bukkit.getPluginManager().registerEvents(ll, Rounded.plugin);
    }
    
    /**
     * Unregisters the {@link Event} {@link Listener}.
     * @param ll the {@link Listener} to unregister.
     */
    protected static void unreg(Listener ll) {
        HandlerList.unregisterAll(ll);
    }
}
