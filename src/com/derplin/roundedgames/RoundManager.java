package com.derplin.roundedgames;

/**
 * The methods that will be called when the round state changes.
 */
public abstract class RoundManager {
    
    /**
     * The method that will be called when the round starts.
     */
    public abstract void roundStart();
    
    /**
     * The method that will be called when the round ends.
     */
    public abstract void roundEnd();
    
    /**
     * Gets the name of the next map to play.
     * This method may be overridden.
     * @return the name of the next map to play or
     *         {@code null} if {@link MapManager} is
     *         enabled and has maps, this will cause it to
     *         pick a random map.
     */
    public String selectNextMap() {
        return null;
    }
    
}